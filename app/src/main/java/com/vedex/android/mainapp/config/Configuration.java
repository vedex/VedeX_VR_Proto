package com.vedex.android.mainapp.config;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class Configuration {
    private static final Configuration configuration = new Configuration();

    private String serverAddress = "http://94.250.252.17:8081";
    private String serverHost = "94.250.252.17";
    private String serverPort = "8081";


    public static Configuration getInstance() {
        return configuration;
    }

    private Configuration() {
    }

    public void loadConfiguration(String fileName) throws IOException {
        try (FileReader r = new FileReader(new File(fileName))) {
            final Properties properties = new Properties();
            properties.load(r);
            this.loadFromProperties(properties);
        }
    }

    private void loadFromProperties(Properties properties) {
        serverHost = properties.getProperty("server.host");
        serverPort = properties.getProperty("server.port");
        serverAddress = "https://" + serverHost + ":" + serverPort;
    }

    public String getServerAddress() {
        return serverAddress;
    }

    public String getServerHost() {
        return serverHost;
    }

    public String getServerPort() {
        return serverPort;
    }
}
