package com.vedex.android.mainapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.vedex.android.mainapp.androidhelpcomponents.GroupListAdapter;
import com.vedex.android.mainapp.dataprocessing.Group;
import com.vedex.android.mainapp.dataprocessing.User;
import com.vedex.android.mainapp.network.APIConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends ConnectionFragment {

    private static int IMG_RESULT = 101;
    ImageView mPhoto;
    TextView mName, mLogin, mEmail;
    RecyclerView mGroupListRv;
    Button mEditProfile;
    private com.github.lzyzsd.circleprogress.DonutProgress mDeterminateBar;
    private LinearLayout mDeterminateBarLayout;
    private LinearLayout mProfileMainLayout;
    private TextView mDeterminateText;
    private String secureUrl;
    private boolean isResponseForImage = false;

    public static void enableDisableView(View view, boolean enabled) {
        view.setEnabled(enabled);
        if ( view instanceof ViewGroup ) {
            ViewGroup group = (ViewGroup)view;
            for ( int idx = 0 ; idx < group.getChildCount() ; idx++ ) {
                enableDisableView(group.getChildAt(idx), enabled);
            }
        }
    }

    private void uploadAvatarToServer() {
        JSONObject request = new JSONObject();
        APIConnection connection = APIConnection.getInstance();
        try {
            String url = "http://37.46.128.101:8081/updatePhoto";
            request.put("new_photo", secureUrl);
            connection.makeRequest(url, request, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMG_RESULT && resultCode == RESULT_OK
                && data != null && getContext() != null && data.getData() != null) {

            MediaManager.get()
                    .upload(data.getData())
                    .option("folder", "VedexVR_UP")
                    .callback(new UploadCallback() {
                        @Override
                        public void onStart(String requestId) {
                            if (ProfileFragment.this
                                    .getView() != null) {
                                mProfileMainLayout.setAlpha(0.3f);
                                enableDisableView(mProfileMainLayout, false);
                                mDeterminateBar = ProfileFragment.this
                                        .getView().findViewById(R.id.determinateBar);
                                mDeterminateBarLayout = ProfileFragment.this
                                        .getView().findViewById(R.id.determinateBarLayout);
                                mDeterminateText = ProfileFragment.this
                                        .getView().findViewById(R.id.determinateText);
                                mDeterminateText.setText(getString(R.string.upload_photo_loading));
                                mDeterminateBar.setVisibility(View.VISIBLE);
                                mDeterminateBarLayout.bringToFront();
                                mDeterminateBarLayout.setVisibility(View.VISIBLE);
                            }
                        }

                        @Override
                        public void onProgress(String requestId, long bytes, long totalBytes) {
                            float c = ((float) bytes / totalBytes)*100f;
                            mDeterminateBar.setProgress(Math.round(c));
                        }

                        @Override
                        public void onSuccess(String requestId, Map resultData) {
                            mDeterminateText.setText(getString(R.string.upload_photo_sync));
                            mDeterminateBar.setVisibility(View.INVISIBLE);
                            secureUrl = (String) resultData.get("secure_url");
                            isResponseForImage = true;
                            uploadAvatarToServer();
                        }

                        @Override
                        public void onError(String requestId, ErrorInfo error) {
                        }

                        @Override
                        public void onReschedule(String requestId, ErrorInfo error) {
                        }
                    })
                    .dispatch();
        } else {
            Toast.makeText(getContext()
                    , getString(R.string.upload_photo_fail)
                    , Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater
            , @Nullable ViewGroup container
            , @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        mProfileMainLayout = rootView.findViewById(R.id.profileMainLayout);
        mPhoto = rootView.findViewById(R.id.profile_picture);
        mPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, IMG_RESULT);
            }
        });
        mName = rootView.findViewById(R.id.profile_user_name_surname);
        mLogin = rootView.findViewById(R.id.profile_user_login);
        mEmail = rootView.findViewById(R.id.profile_user_email);
        mGroupListRv = rootView.findViewById(R.id.my_groups_rv);
        mEditProfile = rootView.findViewById(R.id.edit_profile_button);

        mEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getFragmentManager();
                Fragment fragment = new EditProfileFragment();
                if (fm != null) {
                    fm.beginTransaction().replace(R.id.fragment_container, fragment).
                            addToBackStack("edit_profile").commit();
                }
            }
        });

        User user = User.getInstance();

        if (!user.getPhoto().equals("null") && getContext() != null) {
            mPhoto.setBackground(ContextCompat.getDrawable(getContext()
                    , android.R.color.transparent));
            Glide.with(getContext())
                    .load(user.getPhoto())
                    .into(mPhoto);
        }
        mName.setText(user.getName().concat(" ").concat(user.getSurname()));
        mLogin.setText(user.getUsername());
        if (user.getEmail() == null || user.getEmail().length() == 0) {
            mEmail.setTextColor(Color.RED);
            mEmail.setText(R.string.no_email_alert);
            mEmail.setTextSize(10);
        } else mEmail.setText(user.getEmail());

        loadGroups();

        return rootView;
    }

    private void loadGroups() {
        JSONObject request = new JSONObject();
        APIConnection connection = APIConnection.getInstance();
        try {
            String url = "http://37.46.128.101:8081/getGroupList";
            connection.makeRequest(url, request, ProfileFragment.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPostConnection(String response) {
        Log.d("TAG", response);
        try {
            JSONObject obj = new JSONObject(response);
            if (isResponseForImage && obj.getString("errorcode").equals("0")) {
                isResponseForImage = false;
                enableDisableView(mProfileMainLayout, true);
                mProfileMainLayout.setAlpha(1f);
                mDeterminateBarLayout.setVisibility(View.GONE);
                User.getInstance().setPhoto(secureUrl);

                if (getActivity() != null) {
                    SharedPreferences localData = getActivity()
                            .getSharedPreferences(getActivity().getPackageName()
                                    , Context.MODE_PRIVATE);
                    localData.edit()
                            .putString("photo", secureUrl).apply();
                }
                if (getContext() != null) {
                    mPhoto.setBackground(ContextCompat.getDrawable(getContext()
                            , android.R.color.transparent));
                    Glide.with(getContext())
                            .load(secureUrl)
                            .into(mPhoto);
                    Toast.makeText(getContext()
                            , getString(R.string.upload_photo_ok)
                            , Toast.LENGTH_SHORT).show();
                }
            } else if (!isResponseForImage && obj.getString("errorcode").equals("0")) {
                JSONArray groups = obj.getJSONArray("groups");
                ArrayList<Group> groupArrayList = new ArrayList<>();
                for (int i = 0; i < groups.length(); i++) {
                    JSONObject group = groups.getJSONObject(i);
                    groupArrayList.add(Group.getFromJSON(group));
                }
                RecyclerView.LayoutManager lm = new LinearLayoutManager(getActivity());
                mGroupListRv.setLayoutManager(lm);
                RecyclerView.Adapter mAdapter = new GroupListAdapter(groupArrayList, getActivity());
                mGroupListRv.setAdapter(mAdapter);
            } else {
                Toast.makeText(getActivity(), R.string.api_error, Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
