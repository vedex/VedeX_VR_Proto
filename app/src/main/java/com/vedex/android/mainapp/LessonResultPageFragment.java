package com.vedex.android.mainapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LessonResultPageFragment extends Fragment {

    static final String ARGUMENT_PAGE_NUMBER = "arg_page_number";
    static final String QUESTION_TEXT = "arg_question_text";
    static final String ANSWERS = "arg_answers";
    private int pageNumber;
    private String questionText;
    private JSONArray answers;

    static LessonResultPageFragment newInstance(int page, JSONObject question) {
        LessonResultPageFragment pageFragment = new LessonResultPageFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_PAGE_NUMBER, page);
        try {
            arguments.putString(QUESTION_TEXT, question.getString("QUESTION_TEXT"));
            arguments.putString(ANSWERS, question.getJSONArray("ANSWERS").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pageNumber = getArguments().getInt(ARGUMENT_PAGE_NUMBER);
            questionText = getArguments().getString(QUESTION_TEXT);
            try {
                answers = new JSONArray(getArguments().getString(ANSWERS));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lesson_result_pager_layout, container
                , false);
        TextView tvQuestion = view.findViewById(R.id.tvQuestion);
        tvQuestion.setText(questionText);
        LinearLayout llAnswers = view.findViewById(R.id.llAnswers);
        for (int i=0; i<answers.length(); i++) {
            final Button button = new Button(new ContextThemeWrapper(getContext()
                    , R.style.CommonButton));
            try {
                JSONObject answer = answers.getJSONObject(i);
                String id = String.valueOf(i+1)
                        + String.valueOf(answer.getInt("IS_CORRECT"));
                button.setId(Integer.valueOf(id));
                button.setText(answer.getString("TEXT"));
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int id = view.getId();
                        LessonPageFragment fragment = ((LessonPageFragment)getParentFragment());
                        if (getContext() != null) {
                            Toast.makeText(getContext()
                                    , id % 10 == 1 ? "Верно!" : "Неверно!"
                                    , Toast.LENGTH_SHORT).show();
                        }
                        if (fragment != null) {
                            fragment.moveToNextPage(pageNumber, id % 10);
                        }
                    }
                });
                llAnswers.addView(button);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return view;
    }
}
