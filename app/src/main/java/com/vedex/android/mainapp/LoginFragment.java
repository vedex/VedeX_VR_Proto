package com.vedex.android.mainapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.vedex.android.mainapp.config.Configuration;
import com.vedex.android.mainapp.dataprocessing.User;
import com.vedex.android.mainapp.dataprocessing.Validator;
import com.vedex.android.mainapp.network.APIConnection;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginFragment extends ConnectionFragment {
    private String url = Configuration.getInstance().getServerAddress() + "/doLogin";
    private EditText mUsername;
    private EditText mPassword;
    private String username;
    private String password;

    private SharedPreferences localData;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_login, container, false);
        if (getActivity() != null) {
            localData = getActivity()
                    .getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
        }
        mUsername = view.findViewById(R.id.etUsername);
        mPassword = view.findViewById(R.id.etPassword);
        if (!localData.getString("login", "").equals("")) {
            mUsername.setText(localData.getString("login", ""));
            mUsername.setEnabled(false);
            mPassword.setEnabled(false);
        }
        Button mLogin = view.findViewById(R.id.bLogin);
        TextView mToRegister = view.findViewById(R.id.tvToRegister);
        //TextView mForgotPassword = view.findViewById(R.id.tvForgotPassword);

        mToRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getFragmentManager();
                Fragment fragment = new RegisterFragment();
                if (fm != null) {
                    fm.beginTransaction().replace(R.id.fragment_container, fragment).
                            addToBackStack("login").commit();
                }
            }
        });
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = mUsername.getText().toString();
                password = mPassword.getText().toString();
                if (!Validator.ValidateUsername(username) &&
                        !Validator.ValidateEmail(username) ||
                        !Validator.ValidatePassword(password)) {
                    Activity activity = getActivity();
                    Toast.makeText(activity, R.string.FieldFillingError, Toast.LENGTH_SHORT).show();
                } else {
                    JSONObject request = new JSONObject();
                    try {
                        if (Validator.ValidateUsername(username)) {
                            request.put("login", username);
                        } else {
                            request.put("login", "");
                        }
                        if (Validator.ValidateEmail(username)) {
                            request.put("email", username);
                        } else {
                            request.put("email", "");
                        }
                        if (!localData.getString("login", "").equals("")) {
                            request.put("password"
                                    , localData.getString("password", ""));
                        } else {
                            request.put("password", password);
                        }
                        APIConnection connection = APIConnection.getInstance();
                        connection.makeRequest(url, request, LoginFragment.this);

                    } catch (Exception e) {
                        Log.d("TAG", "Error");
                        e.printStackTrace();
                    }
                }
            }
        });
        /*mForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getFragmentManager();
                Fragment fragment = new ForgotPasswordFragment();
                if (fm != null) {
                    fm.beginTransaction().replace(R.id.fragment_container, fragment).
                            addToBackStack(null).commit();
                }
            }
        });*/
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        JSONObject request = new JSONObject();
        APIConnection connection = APIConnection.getInstance();
        try {
            if (!localData.getString("login", "").equals("")) {
                request.put("login", localData.getString("login", ""));
                request.put("password", localData.getString("password", ""));
                Log.d("SERVER", request.toString());
                connection.makeRequest(url, request, LoginFragment.this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onPostConnection(String response) {
        Log.d("SERVER", response);
        try {
            JSONObject obj = new JSONObject(response);
            if (obj.getString("errorcode").equals("2")) {
                restoreDataFromLocal();
                toMainMenu();
            } else if (obj.getString("errorcode").equals("0")) {
                User user = User.getInstance(obj.getString("login"));
                saveDataLocally(obj);
                user.setId(obj.getInt("id"));
                user.setName(obj.getString("name"));
                user.setSurname(obj.getString("surname"));
                user.setEmail(obj.getString("email"));
                user.setPassword(password);
                user.setPhoto(obj.getString("photo"));
                toMainMenu();
            } else {
                mUsername.setEnabled(true);
                mPassword.setEnabled(true);
                localData.edit().putString("password", "").apply();
                localData.edit().putString("login", "").apply();
                Log.d("errorcode", obj.getString("errorcode"));
                Toast.makeText(getActivity(), R.string.LoginError, Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void saveDataLocally(JSONObject obj) throws JSONException {
        localData.edit().putInt("id", obj.getInt("id")).apply();
        localData.edit().putString("login", obj.getString("login")).apply();
        localData.edit().putString("name", obj.getString("name")).apply();
        localData.edit().putString("surname", obj.getString("surname")).apply();
        localData.edit().putString("email", obj.getString("email")).apply();
        password = localData.getString("password", "").equals("")
                ? password
                : localData.getString("password", "");
        localData.edit().putString("password", password).apply();
        localData.edit().putString("photo", obj.getString("photo")).apply();
    }

    private void restoreDataFromLocal() {
        User user = User.getInstance(localData.getString("login", ""));
        user.setId(localData.getInt("id", -1));
        user.setName(localData.getString("name", ""));
        user.setSurname(localData.getString("surname", ""));
        user.setEmail(localData.getString("email", ""));
        user.setPassword(localData.getString("password", ""));
        user.setPhoto(localData.getString("photo", ""));
    }

    private void toMainMenu() {
        FragmentActivity activity = getActivity();
        Intent intent = new Intent(activity, MainActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
        startActivity(intent);
        if (activity != null) {
            activity.finish();
        }
    }
}
