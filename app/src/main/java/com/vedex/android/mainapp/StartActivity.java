package com.vedex.android.mainapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.vedex.android.mainapp.config.Configuration;

import java.io.IOException;

@SuppressWarnings("FieldCanBeLocal")
public class StartActivity extends AppCompatActivity {

    private static String CONFIG_TAG = "config";
    private static String CONFIG_PATH = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: Add configuration load.
        /*try {
            Configuration.getInstance().loadConfiguration(CONFIG_PATH);
        } catch (IOException e) {
            Log.e(CONFIG_TAG, "Unable to load configuration: " + e);
            onDestroy();
            return;
        }*/
        setContentView(R.layout.activity_start);
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);
        if (fragment == null) {
            fragment = new LoginFragment();
            fm.beginTransaction().add(R.id.fragment_container, fragment).commit();
        }
    }
}
