package com.vedex.android.mainapp;

import android.support.v4.app.Fragment;


public abstract class ConnectionFragment extends Fragment {
    public abstract void onPostConnection(String response);
}
