package com.vedex.android.mainapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.vedex.android.mainapp.dataprocessing.Lesson;
import com.vedex.android.mainapp.dataprocessing.LessonResult;
import com.vedex.android.mainapp.network.APIConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.Iterator;

public class LessonPageFragment extends ConnectionFragment {
    private String mPackageName;
    private long startTime;
    private boolean isStarted = false;
    private Lesson mLesson;
    private int mPassageTime;
    private int mRightPercent = 0;
    private int responseCount = 0;
    private ViewPager pager;
    private JSONArray questionList;
    private int resultQuestionsCount;

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lesson_page, container, false);
        if (getArguments() != null) {
            mLesson = (Lesson) getArguments().getSerializable("lesson");
            if (mLesson != null) {
                mPackageName = mLesson.getPackageName();
            }
        }
        Button mStartLesson = view.findViewById(R.id.start_lesson_button);
        TextView mLessonName = view.findViewById(R.id.lesson_page_lesson_name);
        TextView mSubjectName = view.findViewById(R.id.lesson_page_subject);
        ImageView mImage = view.findViewById(R.id.lesson_page_lesson_picture);
        TextView mDescription = view.findViewById(R.id.lesson_page_lesson_desc);
        TextView mSuccessRate = view.findViewById(R.id.lesson_page_lesson_average);
        TextView mQuestionsNumber = view.findViewById(R.id.lesson_page_lesson_questions_num);

        mLessonName.setText(mLesson.getName());
        mSubjectName.setText(mLesson.getSubject()
                .concat(" " )
                .concat(String.valueOf(mLesson.getForm())));
        mDescription.setText(mLesson.getDescription());
        String successRate = getResources().getString(R.string.success_rate);
        String questionNumber = getResources().getString(R.string.questions_number);
        mSuccessRate.setText(successRate + ": " + mLesson.getSuccessRate() + "%");
        mQuestionsNumber.setText(questionNumber + ": " + mLesson.getQuestionsNumber());

        //new DownloadImageTask(mImage).execute(mLesson.getPhoto());
        if (getContext() != null) {
            Glide.with(getContext()).load(mLesson.getPhoto()).into(mImage);
        }

        mStartLesson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() != null) {
                    if (isPackageInstalled(mPackageName, getActivity().getPackageManager())) {
                        startTime = System.currentTimeMillis();
                        Intent launchIntent = getActivity().getPackageManager().
                                getLaunchIntentForPackage(mPackageName);
                        if (launchIntent != null) {
                            isStarted = true;
                            mLesson.setStarted(true);
                            mLesson.setStartTime(startTime);
                            startActivity(launchIntent);
                        }
                    } else {
                        startActivity(new Intent(Intent.ACTION_VIEW
                                , Uri.parse("market://details?id=" + mPackageName)));
                    }
                }
            }
        });
        return view;
    }

    private boolean isPackageInstalled(String packagename, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packagename, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        isStarted = mLesson.isStarted();
        startTime = mLesson.getStartTime();

        if (responseCount == 2) {
            if (getView() != null) {
                pager.setVisibility(View.GONE);
                getView().findViewById(R.id.svBeforeLesson).setVisibility(View.VISIBLE);
                responseCount = 0;
                return;
            }
        }

        if (isStarted && startTime != -1) {
            if (getActivity() != null
                    && getActivity().getRequestedOrientation() != ActivityInfo
                    .SCREEN_ORIENTATION_PORTRAIT) {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            } else {
                if (getView() != null) {
                    getView().findViewById(R.id.svBeforeLesson).setVisibility(View.GONE);
                }
                mPassageTime = (int) (System.currentTimeMillis() - startTime) / 1000;
                JSONObject request = new JSONObject();
                APIConnection connection = APIConnection.getInstance();
                try {
                    request.put("lesson_id", mLesson.getLessonId());
                    String url1 = "http://37.46.128.101:8081/getLessonQuestions";
                    connection.makeRequest(url1, request, LessonPageFragment.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void sendResults() {
        pager.setVisibility(View.GONE);
        JSONObject request = new JSONObject();
        APIConnection connection = APIConnection.getInstance();
        try {
            request.put("lesson_id", mLesson.getLessonId());
            request.put("passage_time", mPassageTime);
            request.put("right_percent", mRightPercent);
            String url2 = "http://37.46.128.101:8081/addResult";
            connection.makeRequest(url2, request, LessonPageFragment.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void printResults() {
        if (getView() == null) return;
        pager = getView().findViewById(R.id.vpAfterLesson);
        pager.setVisibility(View.VISIBLE);
        LessonResultFragmentPagerAdapter adapter
                = new LessonResultFragmentPagerAdapter(getChildFragmentManager());
        pager.setAdapter(adapter);
    }

    public void moveToNextPage(int currentPage, int wasAnswerCorrect) {
        if (currentPage == resultQuestionsCount - 1) {
            mRightPercent += wasAnswerCorrect;
            mRightPercent = (int)Math.round((double)mRightPercent/resultQuestionsCount*100);
            sendResults();
        } else {
            mRightPercent += wasAnswerCorrect;
            pager.setCurrentItem(currentPage+1);
        }
    }

    private class LessonResultFragmentPagerAdapter extends FragmentPagerAdapter {

        LessonResultFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            try {
                return LessonResultPageFragment.newInstance(position
                        , questionList.getJSONObject(position));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public int getCount() {
            return resultQuestionsCount;
        }
    }

    @Override
    public void onPostConnection(String response) {
        try {
            JSONObject objResponse = new JSONObject(response);

            Log.d("RESPONSE", objResponse.toString());
            if (objResponse.getString("errorcode").equals("0") && responseCount == 0) {
                resultQuestionsCount = objResponse.getJSONObject("questions").length();
                questionList = new JSONArray();
                Iterator it = objResponse.getJSONObject("questions").keys();
                while (it.hasNext()) {
                    JSONObject question = objResponse
                            .getJSONObject("questions")
                            .getJSONObject(it.next().toString());
                    questionList.put(question);
                    it.remove();
                }
                responseCount++;
                printResults();
                return;
            }

            if (objResponse.getString("errorcode").equals("0") && responseCount == 1) {
                Date date = new Date();
                String output = DateFormat.format("dd.MM.yyyy HH:mm:ss", date).toString();
                LessonResult mLessonResult = new LessonResult(mLesson.getSubject(), mLesson.getLessonId(),
                        mLesson.getName(), mLesson.getDescription(), mLesson.getPhoto(), mLesson.getForm(),
                        mLesson.getQuestionsNumber(), mLesson.getSuccessRate(), mPassageTime,
                        mRightPercent, output);
                Bundle b = new Bundle();
                b.putSerializable("Result", mLessonResult);
                FragmentManager fm = getFragmentManager();
                Fragment fragment = new LessonResultFragment();
                fragment.setArguments(b);
                if (fm != null) {
                    fm.beginTransaction()
                            .replace(R.id.fragment_container, fragment)
                            .addToBackStack("result")
                            .commit();
                }
                mRightPercent = 0;
                mLesson.resetLesson();
                pager.setCurrentItem(0);
                responseCount++;
            } else {
                Toast.makeText(getContext(), R.string.api_error, Toast.LENGTH_SHORT).show();
                Log.e("result", objResponse.getString("errorcode"));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
